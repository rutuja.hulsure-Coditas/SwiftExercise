//
//  SelfSizeTableView.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

class SelfSizeTableView: UITableView {
    
    override var intrinsicContentSize: CGSize {
        self.layoutIfNeeded()
        self.layoutSubviews()
        return self.contentSize
    }
    
    override var contentSize: CGSize {
        didSet{
            self.invalidateIntrinsicContentSize()
        }
    }
}
