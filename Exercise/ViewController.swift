//
//  ViewController.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

struct Sections {
    var general : [PatientDetails]?
    var healthDetails : [PatientDetails]?
    var phoneNumber : [PatientDetails]?
    var address : [PatientDetails]?
    var emailAddress : [PatientDetails]?
}
struct PatientDetails {
    var type: String?
    var value: String?
    var placeholder : String?
    var isFavourite: Bool?
    var street1 : String?
    var street2 : String?
    var city : String?
    var postcode : String?
    var state : String?
    var country : String?
    var countryCode : String?
}


class ViewController: UIViewController {

    @IBOutlet weak var createPatientTableView: SelfSizeTableView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addPatientLabel: UILabel!
    @IBOutlet weak var donebutton: UIButton!
    
    var datasource = Sections()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createPatientTableView.delegate = self
        self.createPatientTableView.dataSource = self
        setUpUI()
        
        self.createPatientTableView.register(TextFieldTableViewCell.self)
        self.createPatientTableView.register(LabelTextFieldTableViewCell.self)
        self.createPatientTableView.register(AddPhoneTableViewCell.self)
        self.createPatientTableView.register(AddressTableViewCell.self)
        self.createPatientTableView.register(ButtonTableViewCell.self)
        self.createPatientTableView.register(UINib(nibName: "LabelHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "LabelHeaderView")
        self.datasource.general = [PatientDetails(type: "initial", value: "",placeholder: "Select Title",  isFavourite: nil), PatientDetails(type: "firstName", value: "",placeholder: "First Name", isFavourite: nil), PatientDetails(type: "lastName", value: "",placeholder: "Last Name",  isFavourite: nil)]
        self.datasource.healthDetails = [PatientDetails(type: "Date of Birth", value: "", placeholder: "Set up", isFavourite: nil), PatientDetails(type: "Weight", value: "",placeholder: "Set up", isFavourite: nil), PatientDetails(type: "Height", value: "",placeholder: "Set up", isFavourite: nil)]
        
        
        self.datasource.phoneNumber = [PatientDetails(type: "Add Phone", value: nil, isFavourite: false)]
        self.datasource.address = [PatientDetails(type: "Add address", value: nil, isFavourite: false)]
        self.datasource.emailAddress = [PatientDetails(type: "Add email", value: nil, isFavourite: false)]
    }

    func setUpUI(){
        self.addPatientLabel.text = "Add new patient"
        self.addPatientLabel.font = FontStyle.SFPro_semibold_17
        self.addPatientLabel.textColor = Color.white
        self.cancelButton.isHidden = true
        self.donebutton.titleLabel?.font = FontStyle.SFPro_semibold_16
        self.donebutton.setTitle("Done", for: .normal)
        self.cancelButton.setTitle("Cancel", for: .normal)
     //   self.createPatientTableView.isEditing = true
        self.createPatientTableView.backgroundColor = UIColor(hexString: "#262628", alpha: 1)
    }

}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section{
        case 0:
            return self.datasource.general?.count ?? 0
        case 1:
            return self.datasource.healthDetails?.count ?? 0
        case 2:
            return self.datasource.phoneNumber?.count ?? 0
        case 3:
            return self.datasource.address?.count ?? 0
        case 4:
            return self.datasource.emailAddress?.count ?? 0
        default:
            break
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section{
        case 0:
            let cell = self.createPatientTableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.identifier) as! TextFieldTableViewCell
            cell.setUpCell(info: (self.datasource.general?[indexPath.row])!)
            return cell
        case 1:
            let cell = self.createPatientTableView.dequeueReusableCell(withIdentifier: LabelTextFieldTableViewCell.identifier) as! LabelTextFieldTableViewCell
            cell.setUpCell(info: (self.datasource.healthDetails?[indexPath.row])!)
            return cell
        case 2:
            let cell : ButtonTableViewCell = self.createPatientTableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.button.setTitle(datasource.phoneNumber?[indexPath.row].type, for: .normal)
            //cell.button.setTitleColor(Color.white, for: .normal)
            if indexPath.row != (self.datasource.phoneNumber?.count ?? 0) - 1{
                let cell = self.createPatientTableView.dequeueReusableCell(withIdentifier: AddPhoneTableViewCell.identifier) as! AddPhoneTableViewCell
                cell.setUpCell(info: (self.datasource.phoneNumber?[indexPath.row])!)
                return cell
            }
        case 3:
            let cell : ButtonTableViewCell = self.createPatientTableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.containerView.backgroundColor = Color.grey3
            cell.button.setTitle(datasource.phoneNumber?[indexPath.row].type, for: .normal)
            //cell.button.setTitleColor(Color.white, for: .normal)
            if indexPath.row != (self.datasource.address?.count ?? 0) - 1{
                let cell = self.createPatientTableView.dequeueReusableCell(withIdentifier: AddressTableViewCell.identifier) as! AddressTableViewCell
                cell.configureCell(info: self.datasource.address?[indexPath.row])
                return cell
            }
            else{
                
            }
        case 4:
            let cell : ButtonTableViewCell = self.createPatientTableView.dequeueReusableCell(withIdentifier: ButtonTableViewCell.identifier) as! ButtonTableViewCell
            cell.containerView.backgroundColor = Color.grey4
            cell.button.setTitle(datasource.phoneNumber?[indexPath.row].type, for: .normal)
            //cell.button.setTitleColor(Color.white, for: .normal)
//            if indexPath.row != (self.datasource.emailAddress?.count ?? 0) - 1{
//                let cell = self.createPatientTableView.dequeueReusableCell(withIdentifier: AddPhoneTableViewCell.identifier) as! AddPhoneTableViewCell
//                cell.setUpCell(info: (self.datasource.emailAddress?[indexPath.row])!)
//                return cell
//            }
//            else{
//
//            }
        default:
            break
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        switch indexPath.section{
        case 2 :
            if indexPath.row == (self.datasource.phoneNumber?.count ?? 0) - 1{
                return .insert
            }
            else{
                return .delete
            }
        case 3:
            if indexPath.row == (self.datasource.address?.count ?? 0) - 1{
                return .insert
            }
            else{
                return .delete
            }
        case 4:
            if indexPath.row == (self.datasource.emailAddress?.count ?? 0) - 1{
                return .insert
            }
            else{
                return .delete
            }
        case 0, 1:
            return .none
        default:
            return .none
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .insert{
            if indexPath.section == 2{
                self.datasource.phoneNumber?.append(PatientDetails(type: "Work", value : "", placeholder: "Phone"))
            }
             else if indexPath.section == 3{
                self.datasource.address?.append(PatientDetails(type: "Work", value : "", placeholder: "Phone"))
            }
            else{
                self.datasource.emailAddress?.append(PatientDetails(type: "Work", value : "", placeholder: "Phone"))
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.createPatientTableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = self.createPatientTableView.dequeueReusableHeaderFooterView(withIdentifier: "LabelHeaderView") as? LabelHeaderView
        headerView?.contentView.backgroundColor = Color.grey4
        headerView?.headerLabel.textColor = Color.grey6
        headerView?.headerLabel.font = FontStyle.SFPro_regular_12
        switch section{
        case 0:
            headerView?.headerLabel.text = "GENERAL"
        case 1:
            headerView?.headerLabel.text = "HEALTH DETAILS"
        case 2:
            headerView?.headerLabel.text = "CONTACT DETAILS"
        case 3,4:
            headerView?.headerLabel.isHidden = true
            headerView?.headerViewHtConstraint.constant = 18
        default:
            break
        }
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}
