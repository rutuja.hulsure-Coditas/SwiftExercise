//
//  AddressTableViewCell.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

class AddressTableViewCell: UITableViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textFieldsView: UIView!
    @IBOutlet weak var selectAddressTypeButton: UIButton!
    @IBOutlet weak var street1TextField: UITextField!
    @IBOutlet weak var street2TextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var postCodeTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var starButton: UIButton!
    
    @IBOutlet weak var separatorLineV1: UIView!
    @IBOutlet weak var separatorLineH4: UIView!
    @IBOutlet weak var separatorLineH1: UIView!
    @IBOutlet weak var separatorLineH2: UIView!
    @IBOutlet weak var separatorLineH3: UIView!
    @IBOutlet weak var separatorLineV2: UIView!
    
    static let identifier = String(describing: AddressTableViewCell.self)
    var arrayOfAddressTxtFld = [UITextField]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCell()
        [street1TextField, street2TextField, cityTextField, postCodeTextField, stateTextField].forEach { txtFld in
            txtFld?.backgroundColor = Color.grey4
            txtFld?.textColor = Color.white
            txtFld?.keyboardType = .default
        }
        arrayOfAddressTxtFld = [street1TextField, street2TextField, cityTextField, postCodeTextField, stateTextField]
        postCodeTextField.keyboardType = .numbersAndPunctuation
        street1TextField?.placeHolderWithColor(placeholderText: CommonString.street1, color: Color.grey5)
        street2TextField?.placeHolderWithColor(placeholderText: CommonString.street2, color: Color.grey5)
        cityTextField?.placeHolderWithColor(placeholderText: CommonString.city, color: Color.grey5)
        postCodeTextField?.placeHolderWithColor(placeholderText: CommonString.postalCode, color: Color.grey5)
        stateTextField?.placeHolderWithColor(placeholderText: CommonString.state, color: Color.grey5)

        countryButton.setTitle(CommonString.australia, for: .normal)
    }

    func setUpCell(){
        containerView.backgroundColor = Color.grey4
        textFieldsView.backgroundColor = Color.grey4
        [separatorLineH1, separatorLineH2, separatorLineH3, separatorLineH4, separatorLineV1, separatorLineV2].forEach { separatorLine in
            separatorLine?.backgroundColor = Color.separator2
        }
        starButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
        starButton.tintColor = Color.separator1
        
        selectAddressTypeButton.setTitleColor(Color.blue1, for: .normal)
        selectAddressTypeButton.setTitle(selectContactType[0], for: .normal)
        selectAddressTypeButton.titleLabel?.font = FontStyle.SFPro_regular_15
        selectAddressTypeButton.semanticContentAttribute = UIApplication.shared
            .userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        let chevronImg = Image.chevronRight?.withRenderingMode(.alwaysTemplate)
        selectAddressTypeButton.setImage(chevronImg, for: .normal)
        selectAddressTypeButton.tintColor = Color.grey5
        
        countryButton.setTitleColor(Color.white, for: .normal)
        countryButton.titleLabel?.font = FontStyle.SFPro_regular_17
        countryButton.setTitle(CommonString.australia, for: .normal)
        postCodeTextField.setLeftPaddingPoints(10)
    }
    
    func configureCell(info : PatientDetails?){
        selectAddressTypeButton.setTitle(info?.type, for: .normal)
        street1TextField.text = info?.street1
        street2TextField.text = info?.street2
        cityTextField.text = info?.city
        postCodeTextField.text = info?.postcode
        stateTextField.text = info?.state
        countryButton.setTitle(info?.country, for: .normal)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
