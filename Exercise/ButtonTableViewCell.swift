//
//  ButtonTableViewCell.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

class ButtonTableViewCell: UITableViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var button: UIButton!
    
    static let identifier = String(describing: ButtonTableViewCell.self)
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
