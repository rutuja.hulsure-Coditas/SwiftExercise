
import Foundation
import UIKit

protocol ReusableView: AnyObject {
    static var identifier: String { get }
}

extension ReusableView where Self: UIView {
    static var identifier: String {
        return String(describing: self)
    }
}

protocol NibLoadableView: AnyObject {
    static var nibName: String { get }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}

extension UICollectionView {
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView {
        register(T.self, forCellWithReuseIdentifier: T.identifier)
    }
    
    func register<T: UICollectionViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellWithReuseIdentifier: T.identifier)
    }
    
    func registerSupplementaryView<T: UICollectionReusableView>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.identifier)")
        }
        return cell
    }
    
    func dequeueReusableSupplementaryView<T: UICollectionReusableView>(kind: String, for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.identifier)")
        }
        return cell
    }
    
//    func refreshData() {
//        DispatchQueue.main.async {
//            self.reloadData()
//            self.refreshControl?.endRefreshing()
//            if let pullToRefreshView = self.pullToRefreshView { pullToRefreshView.stopAnimating() }
//        }
//    }
}

extension UITableView {
    
    func register<T: UITableViewCell>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forCellReuseIdentifier: T.identifier)
    }
    func register<T: UITableViewHeaderFooterView>(_: T.Type) where T: ReusableView, T: NibLoadableView {
        let bundle = Bundle(for: T.self)
        let nib = UINib(nibName: T.nibName, bundle: bundle)
        register(nib, forHeaderFooterViewReuseIdentifier: T.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T where T: ReusableView {
        guard let cell = dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.identifier)")
        }
        return cell
    }
    func dequeueReusableHeaderFooterCell<T: UITableViewHeaderFooterView>() -> T where T: ReusableView {
        guard let cell = dequeueReusableHeaderFooterView(withIdentifier: T.identifier) as? T else {
            fatalError("Could not dequeue cell with identifier: \(T.identifier)")
        }
        return cell
    }
    
//    func refreshData() {
//        DispatchQueue.main.async {
//            self.reloadData()
//            self.refreshControl?.endRefreshing()
//            if let pullToRefreshView = self.pullToRefreshView { pullToRefreshView.stopAnimating() }
//        }
//    }
}

extension UITableViewCell {
    static let blankCell = UITableViewCell(frame: CGRect.zero)
    
    @objc func willUpdateUI() {
        setNeedsLayout()
        layoutIfNeeded()
    }
}

extension UICollectionViewCell {
    @objc func willUpdateUI() {
        setNeedsLayout()
        layoutIfNeeded()
    }
}

extension UIScrollView {
    
//    func setUpRefreshControlUI() {
//        if let pullToRefresh = self.pullToRefreshView {
//            let colour: UIColor = .flWhite
//            pullToRefresh.arrowColor = colour
//            pullToRefresh.textColor = colour
//            pullToRefresh.activityIndicatorViewStyle = .medium
//        }
//    }
    
    func scrollToBottom(animated: Bool) {
        let bottomOffset = CGPoint(x: contentOffset.x,
                                   y: contentSize.height - bounds.height + adjustedContentInset.bottom)
        setContentOffset(bottomOffset, animated: animated)
    }
    
    func scrollToTop(animated: Bool, withOffset offset: CGFloat = 0) {
        setContentOffset(CGPoint(x: 0, y: offset), animated: animated)
    }
}

//extension UIViewController {
//    func presentBottomSheet(_ bottomSheet: BottomSheetVCViewController, completion: (() -> Void)?) {
//        self.present(bottomSheet, animated: false, completion: completion)
//    }
//}
