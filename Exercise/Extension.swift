//
//  Extension.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import Foundation
import UIKit

extension UITextField{
    func placeHolderWithColor(placeholderText : String, color : UIColor){
        self.attributedPlaceholder = NSAttributedString(string: placeholderText, attributes: [NSAttributedString.Key.foregroundColor: color])
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
    
    func setPleaceholderColor(color : UIColor){
        var placeholder = String()
        if self.placeholder != nil{
            placeholder = self.placeholder!
        }else{
            placeholder = ""
        }
        self.attributedPlaceholder = NSAttributedString(string:placeholder,
                                                        attributes:[NSAttributedString.Key.foregroundColor:color])
    }
    
    func setKeyboardWithCapsLock() {
            autocapitalizationType = .words
            keyboardType = .asciiCapable
        }
}


extension UIColor{
    convenience init(hexString:String,alpha:CGFloat=1.0) {
        var hexFormatted: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()

        if hexFormatted.hasPrefix("#") {
            hexFormatted = String(hexFormatted.dropFirst())
        }

        assert(hexFormatted.count == 6, "Invalid hex code used.")

        var rgbValue: UInt64 = 0
        Scanner(string: hexFormatted).scanHexInt64(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }
}
