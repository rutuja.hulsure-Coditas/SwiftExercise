//
//  AddPhoneTableViewCell.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

class AddPhoneTableViewCell: UITableViewCell, NibLoadableView, ReusableView  {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var contactTypeButton: UIButton!
    @IBOutlet weak var starButton: UIButton!
    
    static let identifier = String(describing: AddPhoneTableViewCell.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // containerView.backgroundColor = Color.grey3
        contactTypeButton.setTitleColor(Color.blue1, for: .normal)
        starButton.setImage(UIImage(systemName: "star.fill"), for: .normal)
    }

    func setUpCell(info : PatientDetails){
        textField.placeholder = info.placeholder
        contactTypeButton.setTitle(info.type, for: .normal)
        textField.text = info.value
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
