//
//  TextFieldTableViewCell.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell, NibLoadableView, ReusableView {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textField: UITextField!
    
    static var identifier = String(describing: TextFieldTableViewCell.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        containerView.backgroundColor = Color.grey4
        textField.backgroundColor = Color.grey4
        textField.textColor = Color.white
        textField.attributedPlaceholder = NSAttributedString(string: "placeholder text", attributes: [NSAttributedString.Key.foregroundColor: Color.grey5])
    }

    func setUpCell(info : PatientDetails){
        textField.placeholder = info.placeholder
        textField.text = info.value
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
