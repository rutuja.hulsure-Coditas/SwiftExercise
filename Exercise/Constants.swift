//
//  Constants.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import Foundation
import UIKit

struct Color{
    static let grey1 = UIColor(hexString: "#000000")
    static let grey2 = UIColor(hexString: "#121212")
    static let grey3 = UIColor(hexString: "#262628")
    static let grey4 = UIColor(named: "#2C2C2E")
    static let grey5 = UIColor(hexString: "#7D7D7D")
    static let white = UIColor(hexString: "#FFFFFF")
    static let grey6 = UIColor(hexString: "#FFFFFF", alpha: 0.6)
    static let blue1 = UIColor(hexString: "#1994FB")
    static let separator1 = UIColor(hexString: "#FFFFFF", alpha: 0.2)
    static let separator2 = UIColor(hexString: "#FFFFFF",alpha: 0.04)
    static let failure = UIColor(hexString: "#EB5545")
    static let alertBackground = UIColor(hexString: "#292A30")
    static let pickerBackground = UIColor(named: "pickerBackground")
    static let glass1 = UIColor(named: "glass1")
}

struct FontStyle{
    static let SFPro_semibold_16 = UIFont.systemFont(ofSize: 16, weight: .semibold)
    static let SFPro_semibold_17 = UIFont.systemFont(ofSize: 17, weight: .semibold)
    static let SFPro_semibold_22 = UIFont.systemFont(ofSize: 22, weight: .semibold)

    static let SFPro_regular_12 = UIFont.systemFont(ofSize: 12, weight: .regular)
    static let SFPro_regular_13 = UIFont.systemFont(ofSize: 13, weight: .regular)
    static let SFPro_regular_15 = UIFont.systemFont(ofSize: 15, weight: .regular)
    static let SFPro_regular_17 = UIFont.systemFont(ofSize: 17, weight: .regular)
    static let SFPro_medium_17 = UIFont.systemFont(ofSize: 17, weight: .medium)
    static let SFPro_medium_28 = UIFont.systemFont(ofSize: 28, weight: .medium)
    static let SFPro_medium_22 = UIFont.systemFont(ofSize: 22, weight: .medium)
    static let SFPro_medium_34 = UIFont.systemFont(ofSize: 34, weight: .medium)
    static let SFPro_semibold_34 = UIFont.systemFont(ofSize: 34, weight: .semibold)
    static let SFPro_regular_16 = UIFont.systemFont(ofSize: 16, weight: .regular)
    static let SFPro_regular_14 = UIFont.systemFont(ofSize: 14, weight: .regular)
    static let SFPro_semibold_42 = UIFont.systemFont(ofSize: 42, weight: .semibold)

}

var selectContactType = ["Work","Home","Other"]
var titleType = ["Mr.", "Ms.", "Miss", "Mrs.", "Sr.", "Prof.", "Dr.", "Master"]

struct CommonString{
    static let phone = "Phone"
    static let done = "Done"
    static let cancel = "Cancel"
    static let email = "Email"
    static let delete = "Archive"
    static let edit = "Edit"
    static let label = "Label"
    static let selectScan = "Select scan"
    static let deleteScan = "Archive scan"
    static let createPatientTitle = "Add new patient"
    static let retakeScan = "Retake scan"
    static let basicDetails = "Basic Details"
    static let tapOnAreaToScan = "Tap on the marked areas to scan"
    static let discardChanges = "Discard Changes"
    static let scans = "Scans"
    static let media = "Media"
    static let back = "Back"
    static let launch = "launch"
    static let general = "GENERAL"
    static let healthDetails = "HEALTH DETAILS"
    static let contactDetails = "CONTACT DETAILS"
    static let error = "ERROR"
    static let initNavBarIdentifier = "initNavBar"
    static let street1 = "Street1"
    static let street2 = "Street2"
    static let city = "City"
    static let postalCode = "Postcode"
    static let state = "State"
    static let dateFolderAlreadyExistErrorMsg = "Folder for the selected date already exists."
    static let select = "Select"
    static let archive = "Archive"
    static let emailOrUsername = "Email/ Username"
    static let password = "Password"
    static let selectOrg = "Select Organization"
    static let noScanFound = "No Scans Found"
    static let noScanSubLabel = "Scans taken for the patient will appear here."
    static let deleteScanMessage = "Scans archived from the device can be recovered through the web portal."
    static let overrideScanMessage = "The existing scan shall be deleted. Do you wish to continue?."
    static let dontHaveAnAcc = "Don't have an account?"
    static let alreadyHaveAnAccount = "Already have an account?"
    static let linkSent = "Link Sent"
    static let sentTo = "Reset password link has been successfully sent to your registered email"
    static let backToLogin = "Back To Login"
    static let predFormat = "SELF MATCHES %@"
    static let accountCreated = "Account Created"
    static let  captionAccCreated = "You are all set, you can now setup your account"
    static let  getStarted = "Get Started"
    static let selectItems = "Select Items"
    static let sortA_Z = "Alphabetically (A-Z)"
    static let sortZ_A = "Alphabetically (Z -A)"
    static let newestScannedFirst = "Newest Patients"
    static let oldestScannedFirst = "Oldest Patients"
    static let titleMr = "Mr."
    static let name = "Name"
    static let title = "Title"
    static let firstName = "First Name"
    static let lastName = "Last Name"
    static let ownersUsername = "Owner User Name"
    static let ownersEmail = "Owner Email"
    static let organizationName = "Organization Name"
    static let organizationEmail = "Organization Email"
    static let organizationShortName = "Organization Short Name"
    static let organizationDetails = "ORGANIZATION DETAILS"
    static let ownersDetails = "OWNER DETAILS"
    static let dropDownMenu = "DropDownMenu"
    static let  sections = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    static let titleList = ["Mr.", "Ms.", "Miss", "Mrs.", "Sr.", "Prof.", "Dr.", "Master"]
    static let orgList = ["Coditas","Accenture","Wipro","Tcs","Capgemini"]
    static let customNavbar = "CustomNavigationBar"
    static let textfieldWithDownArrow = "TextFieldWithDownArrow"
    static let customTextField = "CustomTextField"
    static let mediaTypes = ["public.image", "public.movie"]
    static let error2 = "Error"
    static let cfbundleShortVersionString = "CFBundleShortVersionString"
    static let username = "username"
    static let organization1 = "organization"
    static let password1 = "password"
    static let australia = "Australia"
    static let charactersCapAndSmall = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ "
    static let numbers = "0123456789+"
    static let textColor = "textColor"
    static let dialCode = "dial Code"
}


struct Image{
    static let chevronRight = UIImage(systemName: "chevron.right")
}
