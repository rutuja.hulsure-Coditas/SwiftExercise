//
//  LabelTextFieldTableViewCell.swift
//  Exercise
//
//  Created by coditas on 05/09/23.
//

import UIKit

class LabelTextFieldTableViewCell: UITableViewCell, NibLoadableView, ReusableView  {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var label: UILabel!
    
    static let identifier = String(describing: LabelTextFieldTableViewCell.self)
    override func awakeFromNib() {
        super.awakeFromNib()
        //containerView.backgroundColor = Color.grey3
        label.font = FontStyle.SFPro_regular_15
        label.textColor = Color.white
        textField.placeholder = "Set Up"
        textField.backgroundColor = Color.grey4
        textField.textColor = Color.white
        textField.attributedPlaceholder = NSAttributedString(string: "placeholder text", attributes: [NSAttributedString.Key.foregroundColor: Color.grey5])
    }

    func setUpCell(info : PatientDetails){
        label.text = info.type
        textField.text = info.value
        textField.placeholder = info.placeholder
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
